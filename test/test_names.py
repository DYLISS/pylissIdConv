# -*- coding: utf-8 -*-
# Copyright 2017 Pierre Vignet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Unit tests to support the decoding of the names of SBML entities.

"""

from pylissidconv \
    import clean_name, \
           html_entities_to_utf8, \
           html_entities_to_names


def test_clean_name():
    """Test convertion of html entities in a string to their ascii name.
    """

    found = clean_name('_2-Oxo-3-hydroxy-4-phosphobutanoate')
    assert found == '2-Oxo-3-hydroxy-4-phosphobutanoate'

    found = clean_name('a &beta;-D-galactosyl-(1,4)')
    assert found == 'a beta-D-galactosyl-(1,4)'

    found = clean_name('galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')
    assert found == "galactosaminyl-alpha1,3-N,N'-diacetylbacillosaminyl-alpha1-di"


def test_html_entities_to_names():
    """Test decoding of names whose special characters are encoded in 2 passes.

    .. note:: this function is used internally by clean_name()
    """

    found_1_pass = html_entities_to_names('galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')
    assert found_1_pass == "galactosaminyl-&alpha;1,3-N,N'-diacetylbacillosaminyl-&alpha;1-di"

    found_2_pass = html_entities_to_names(found_1_pass)
    assert found_2_pass == "galactosaminyl-alpha1,3-N,N'-diacetylbacillosaminyl-alpha1-di"


def test_html_entities_to_utf8():
    """"Test convertion of html entities in a string to utf8 characters."""

    found = html_entities_to_utf8('a &beta;-D-galactosyl-(1,4)')
    # Beware with unicode in Python2.
    # Use '.decode('utf-8')' or put 'u' before string
    assert found == u'a β-D-galactosyl-(1,4)'

    found_1_pass = html_entities_to_utf8('galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')
    assert found_1_pass == u"galactosaminyl-&alpha;1,3-N,N'-diacetylbacillosaminyl-&alpha;1-di"

    found_2_pass = html_entities_to_utf8(found_1_pass)
    assert found_2_pass == u"galactosaminyl-α1,3-N,N'-diacetylbacillosaminyl-α1-di"

