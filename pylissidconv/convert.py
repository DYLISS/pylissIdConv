# -*- coding: utf-8 -*-
# Copyright 2017 Pierre Vignet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
This module includes functions used to decode(clean)/encode
identifiers & names from Metacyc & Bigg SBML files.

.. sectionauthor:: <pierre.vignet@irisa.fr>
"""

# Standard imports
import re
import hashlib

# Custom imports
from pylissidconv import elementnames_handling as fh

__all__ = ['encode_metacyc', 'decode_metacyc', 'decode_bigg', 'encode_bigg',
           'clean_name', 'universal_decoder', 'universal_encoder']

## Useful functions ############################################################
def encode_any(identifier):
    """Encode identifier by encoding in unicode the characters that are not
    allowed in SBML format.

    :param arg1: decoded id
    :type arg1: <str>
    :return: encoded id
    :rtype: <str>

    """
    def unicode_replace(match):
        return "__" + str(ord(match.group(1))) + "__"

    reg_expr_non_unicode = re.compile(r'(\W)')
    return reg_expr_non_unicode.sub(unicode_replace, identifier)


def add_compartment(compartment):
    """Add compartment information to an idenfier during encoding

    .. note:: Add '_compartment' if the given parameter is not None.

    :param arg1: id without compartment info
    :type arg1: <str>
    :return: id with '_compartment' at the end if compartment is not None
    :rtype: <str>

    """
    return '_' + compartment if (compartment is not None) else ''


## Metacyc identifiers #########################################################
def encode_metacyc(identifier, compartment=None, **kwargs):
    """Encode identifier to Metacyc SBML format

        - encode non unicode word character to numeric version
        - add prefix '_' if first char is a digit
        - add suffix '_compartment' if the given parameter is not None.

    :param arg1: decoded id
    :param arg2: compartment info to be concatenated (facultative)
    :type arg1: <str>
    :type arg2: <str>
    :return: encoded id
    :rtype: <str>

    """

    new_id = encode_any(identifier)
    # Add '_' if first char is a digit
    try:
        int(new_id[0])
        return '_' + new_id + add_compartment(compartment)
    except ValueError:
        return new_id + add_compartment(compartment)


def decode_metacyc(identifier):
    """Clean Metacyc dirty identifiers from SBML

        - unicode encoded chars to utf-8
        - strip ALL (!) '_' at the begining

    :param arg1: encoded id
    :type arg1: <str>
    :return: tuple with decoded id and compartment.
        Please note that compartment could be None.
    :rtype: <tuple <str>, <str>>

    """

    def utf8_replace(match):
        return chr(int(match.group(1)))

    reg_expr_unicode = re.compile(r'__(\d+)__')
    # Decode unicode encoding
    # Strip all '_' at the begining
    proper_id = reg_expr_unicode.sub(utf8_replace, identifier).lstrip('_')

    # Handle compartment info
    # PS: You can extract compartment info here:
    # \D : All unicode that is not a digit => experimental !
    reg_expr_compartment = re.compile(r'(?P<id>.*)_(?P<compartment>\D)')

    try:
        match_compartment = reg_expr_compartment.match(proper_id)
        return match_compartment.group('id'), \
               match_compartment.group('compartment')
    except AttributeError:
        return proper_id, None


## BiGG identifiers ############################################################
def decode_bigg(identifier):
    """Clean BiGG dirty identifiers from SBML

    ``identifier.lstrip('M_').rstrip('_e').rstrip('_b').rstrip('_c').replace('DASH', '')``
        - Remove '__DASH__' pattern
        - Remove `'M_'` or `'R_'` prefix
        - Remove '_x' suffix of compartment info

    .. warning:: We assume that any compartment is composed of:
        - 1 character ONLY
        - the unique character IS NOT a digit

    :param arg1: encoded id
    :type arg1: <str>
    :return: tuple of decoded id and compartment,
        None in case of failure (bad prefix). that compartment could be None.
    :rtype: <tuple <str>, <str>> or None

    """

    # Brutal replacement of '_DASH_' pattern
    identifier = identifier.replace('_DASH_', '__')

    # PS: Reactions have not compartment information
    # EDIT: False ! Exchange reactions have this information !
    reg_expr = re.compile(r'(?P<type>[MR]_)(?P<else>.*)')

    # PS: You can extract compartment info here:
    # \D : All unicode that is not a digit => experimental !
    reg_expr_compartment = re.compile(r'(?P<id>.*)_(?P<compartment>\D)')

    match = reg_expr.match(identifier)
    if match is None:
        return

    if match.group('type') == 'M_':
        # Metabolite

        match_compartment = reg_expr_compartment.match(match.group('else'))

        return match_compartment.group('id'), \
               match_compartment.group('compartment')

    elif match.group('type') == 'R_':
        # Reaction
        try:
            # Reaction with compartment info (exchange reactions only)
            match_compartment = reg_expr_compartment.match(match.group('else'))
            return match_compartment.group('id'), \
                   match_compartment.group('compartment')
        except AttributeError:
            return match.group('else'), None

    return


def encode_bigg(identifier, metabolite=True, compartment=None, **kwargs):
    """Encode identifier to Metacyc SBML format

        - encode non unicode word character to numeric version
        - add prefix `'M_'` in case of metabolite=True (default)
        - add prefix `'R_'` in case of metabolite=False
        - add suffix '_compartment' if the given parameter is not None.

    :param arg1: decoded id
    :param arg2: True (default) if id is a metabolite; False if it is a reaction
    :param arg3: compartment info to be concatenated (facultative)
    :type arg1: <str>
    :type arg2: <boolean>
    :type arg3: <str>
    :return: encoded id
    :rtype: <str>

    """

    if metabolite:
        prefix = 'M_'
    else:
        prefix = 'R_'

    # Add prefix
    return prefix + encode_any(identifier) + add_compartment(compartment)



### Unknown identifiers ########################################################
def decode_unknown(identifier):
    """Clean Unknown identifiers from SBML.

    We assume that if decode_bigg() func returns None, the identifier is
    encoded with "Metacyc" rules.

    Some Metacyc identifiers begin with `'M__'` and `'R__'`, while `'M_'`
    and `'R_'` are BiGG prefixes.
    If the given identifier begins with those prefixes,
    we switch on "Metacyc" rules regardless of the return of decode_bigg().

    :param arg1: encoded id
    :type arg1: <str>
    :return: tuple of decoded id and compartment,
        None in case of failure (bad prefix). that compartment could be None.

    """

    purpose = decode_bigg(identifier)

    return decode_metacyc(identifier) \
        if (purpose is None) \
        or (identifier.startswith('M__')) \
        or (identifier.startswith('R__')) \
        else purpose


def encode_unknown(identifier, **kwargs):
    """Encode identifier to Metacyc SBML format by default.
    See encode_bigg() and encode_metacyc() for further information.

    :param arg1: decoded id
    :param arg2: compartment info to be concatenated (facultative)
    :type arg1: <str>
    :type arg2: <str>
    :return: encoded id
    :rtype: <str>

    """

    return encode_metacyc(identifier, **kwargs)


### Clean SBML names ###########################################################
def clean_name(identifier):
    """Convert html entities in a string to their ascii name.

    :Example:
        'a &beta;-D-galactosyl-(1,4)' => 'a beta-D-galactosyl-(1,4)'

    .. note:: For more examples & explanations, please take a look
        at the doc of `html_entities_to_names()` function.

    """

    return fh.html_entities_to_names(
        fh.html_entities_to_names(identifier).lstrip('_')
    )

### Universal functions ########################################################
def universal_decoder(identifier, source_database=None):
    """Handle the decoding of identifier according to the given database.

    See decode_bigg(), decode_metacyc() and decode_unknown()
    for further information.

    .. note:: If the returned identifier has a length greater than 80
        characters, it will be transformed in md5 hex digest.

    """

    decode_funcs = {"bigg": decode_bigg,
                    "metacyc": decode_metacyc,
                    None: decode_unknown,
                   }

    ret = decode_funcs.get(source_database, decode_unknown)(identifier)

    # Limit length of id. User ids are targeted here.
    # The database field does not take above 80 chars.
    if ret is not None and len(ret[0]) > 70:
        ret = list(ret) # yes it is ugly
        hash = hashlib.md5()
        hash.update(ret[0].encode('utf8'))
        ret[0] = hash.hexdigest()
        ret = tuple(ret) # Useless ? => uniformization of the module

    return ret


def universal_encoder(identifier, source_database=None, **kwargs):
    """Handle the encoding of identifier according to the given database.

    See encode_bigg(), encode_metacyc() and encode_unknown()
    for further information.

    """

    encode_funcs = {"bigg": encode_bigg,
                    "metacyc": encode_metacyc,
                    None: encode_unknown,
                   }

    return encode_funcs.get(source_database, encode_unknown)(identifier, **kwargs)


if __name__ == "__main__":

    # Very ugly id from userland
    print(universal_decoder('S__40_15S_41__45_15_45_Hydroxy_45_5_44_8_44_11_45_cis_45_13_45_trans_45_eicosatetraenoate_c'))

    # Universal decoder
    print(universal_decoder('_4M__45__TOTO', "bigg"))
    print(universal_decoder('_4M__45__TOTO', ))
    print(universal_decoder('M_tartr__L_e', ))
    print(universal_decoder('M__45__CRESOL__45__METHYLCATECHOL__45__RXN', ))
    print(universal_decoder('R__45__2__45__HYDROXYGLUTARATE', ))
    print(universal_encoder('4M-TOTO_c'))
    print(universal_encoder('4M-TOTO_c', 'metacyc'))
    print(universal_encoder('|M-TOTO', source_database="metacyc", compartment='c'))
    print(universal_encoder('12PPDt', source_database="bigg", metabolite=False, compartment='c'))
    print(universal_encoder('12PPDt', source_database=None, metabolite=False, compartment='c'))

    # Handle BiGG identifiers
    print(decode_bigg('M_tartr__L_e'))
    print(decode_bigg('M_tartr_DASH_L_e'))
    print(decode_bigg('M_taur_c'))
    print(decode_bigg('M_12ppd_DASH_S_c'))
    print(decode_bigg('R_12PPDt'))
    print(decode_bigg('R_EX_12ppd__S_e'))
    print(decode_bigg('R_MAN6Pt6_2'))

    # Should return None (Metacyc ids passed to a bigg function)
    print(decode_bigg('_4M__45__TOTO_c'))
    print(decode_bigg('2-oxoglutarate-dehydrogenase-E2-lipoyl-c'))
    print(decode_bigg('4M-TOTO_c'))
    # 2 very problematic elements (Metacyc) that are handle despite everything
    print(decode_bigg('M__45__CRESOL__45__METHYLCATECHOL__45__RXN'))
    print(decode_bigg('R__45__2__45__HYDROXYGLUTARATE'))

    # Handle Metacyc identifiers
    print(decode_metacyc('_4M__45__TOTO_c'))
    print(decode_metacyc('2-oxoglutarate-dehydrogenase-E2-lipoyl-c'))
    print(decode_metacyc('_9__45__cis__45__Epoxycarotenoids'))
    print(encode_metacyc('4M-TOTO_c'))
    print(encode_metacyc('|M-TOTO', compartment='c'))
    print(encode_bigg('12PPDt', metabolite=False, compartment='c'))


    print(clean_name('_2-Oxo-3-hydroxy-4-phosphobutanoate'))
    print(fh.html_entities_to_names(fh.html_entities_to_names('galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')))

