
all: clean compile sdist

clean:
	@echo Clean Python build dir...
	python setup.py clean --all
	@echo Clean Python distribution dir...
	@-rm -rf dist

compile:
	@echo Building the library...
	python setup.py build

sdist:
	@echo Building the distribution package...
	python setup.py sdist

install:
	@echo Install the package...
	python setup.py install --record files.txt

uninstall: files.txt
	@echo Uninstalling the package...
	cat files.txt | xargs rm -rf
	rm files.txt

unit_tests:
	@echo Launch unit tests
	python setup.py test
	
test_register:
	python setup.py register -r pypitest

test_install:
	python setup.py sdist upload -r pypitest
	pip install -U -i https://testpypi.python.org/pypi pylissIdConv
